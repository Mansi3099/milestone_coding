﻿using Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Features.Commands;
using Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Features.Queries;
using Mansi_Milestone_9Sept__2_55pm_.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
//This is an Empty Api Controller

namespace Mansi_Milestone_9Sept__2_55pm_.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        
        private IMediator _mediator;

        public HomeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]

        public async Task<IEnumerable<Product>> GetProducts()
        {
            return await _mediator.Send(new GetAllProductsQuery());
        }
        [HttpGet]
        public async Task<IActionResult> GetAllCategories()
        {
            return Ok(_mediator.Send(new GetAllCategoriesQuery()));
        }
        [HttpGet]
        public async Task<IActionResult> GetProductDetailsById(int id)
        {
            return Ok(_mediator.Send(new GetProductDetailsByIdQuery() { Productid = id }));
        }
        [HttpGet]
        public async Task<IActionResult> OrderInfo(int id)
        {
            return Ok(_mediator.Send(new GetOrderInfoQuery() { Userid = id }));
        }
        [HttpPost]
        public async Task<ProductOrder> PlaceOrder(int userid, int productid)
        {
            return await _mediator.Send(new PlaceOrderCommand { Userid = userid, Productid = productid });
        }
    }
}

    

