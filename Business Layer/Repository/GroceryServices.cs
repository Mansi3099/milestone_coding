﻿using Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Persistence;
using Mansi_Milestone_9Sept__2_55pm_.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Repository
{
    public class GroceryServices : IGroceryServices
    {
        private StoreDBContext _dbContext;
        public GroceryServices(StoreDBContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IEnumerable<Categories> GetAllCategories()
        {
            return _dbContext.Categories.ToList();
        }

        public IEnumerable<Product> GetAllProducts()
        {
            return _dbContext.Product.ToList();
        }

        public Product GetProductDetailsById(int id)
        {
            return _dbContext.Product.SingleOrDefault(x => x.Productid == id);
        }

        public IEnumerable<ProductOrder> OrderInfo(int id)
        {
            return _dbContext.ProductOrder.Where(x => x.Userid == id);
        }

        public ProductOrder PlaceOrder(int userid, int id)
        {
            var order = new ProductOrder()
            {
                Orderid = _dbContext.ProductOrder.Max(x => x.Orderid) + 1,
                Productid = id,
                Userid = userid,
                User = _dbContext.ApplicationUser.SingleOrDefault(x => x.Id == userid),
                Product = _dbContext.Product.SingleOrDefault(x => x.Productid == id)

            };
            _dbContext.ProductOrder.Add(order);
            _dbContext.SaveChanges();
            return order;
        }
    }
}
