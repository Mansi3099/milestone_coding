﻿using Mansi_Milestone_9Sept__2_55pm_.Entities;
using System.Collections.Generic;

namespace Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Persistence
{
    public interface IGroceryServices
    {
        
        IEnumerable<Product> GetAllProducts();
        IEnumerable<Categories> GetAllCategories();

        ProductOrder PlaceOrder(int userid, int id);
        Product GetProductDetailsById(int id);
        IEnumerable<ProductOrder> OrderInfo(int id);

    }
}

