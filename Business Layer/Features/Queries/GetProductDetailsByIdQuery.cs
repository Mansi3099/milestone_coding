﻿using Mansi_Milestone_9Sept__2_55pm_.Entities;
using MediatR;

namespace Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Features.Queries
{
    public class GetProductDetailsByIdQuery:IRequest<Product>
    {
        public int Productid { get; set; }
    }
}
