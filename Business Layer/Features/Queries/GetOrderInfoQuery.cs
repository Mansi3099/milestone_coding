﻿using Mansi_Milestone_9Sept__2_55pm_.Entities;
using MediatR;
using System.Collections.Generic;

namespace Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Features.Queries
{
    public class GetOrderInfoQuery:IRequest<IEnumerable<ProductOrder>>
    {
        public int Userid { get; set; }
    }
}
