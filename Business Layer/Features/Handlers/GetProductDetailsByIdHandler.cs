﻿using Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Features.Queries;
using Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Persistence;
using Mansi_Milestone_9Sept__2_55pm_.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Features.Handlers
{
    public class GetProductDetailsByIdHandler : IRequestHandler<GetProductDetailsByIdQuery, Product>
    {
        private IGroceryServices _gs;

        public GetProductDetailsByIdHandler(IGroceryServices gs)
        {
            _gs = gs;
        }

        public async Task<Product> Handle(GetProductDetailsByIdQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_gs.GetProductDetailsById(request.Productid));
        }
    }
}
