﻿using Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Features.Queries;
using Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Persistence;
using Mansi_Milestone_9Sept__2_55pm_.Entities;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Features.Handlers
{
    public class GetAllProductsHandler : IRequestHandler<GetAllProductsQuery, IEnumerable<Product>>
    {
        private IGroceryServices _gs;

        public GetAllProductsHandler(IGroceryServices gs)
        {
            _gs = gs;
        }

        public async Task<IEnumerable<Product>> Handle(GetAllProductsQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_gs.GetAllProducts());
        }
    }
}
