﻿using Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Features.Queries;
using Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Persistence;
using Mansi_Milestone_9Sept__2_55pm_.Entities;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Features.Handlers
{
    public class GetOrderInfoHandler : IRequestHandler<GetOrderInfoQuery, IEnumerable<ProductOrder>>
    {
        private IGroceryServices _gs;

        public GetOrderInfoHandler(IGroceryServices gs)
        {
            _gs = gs;
        }

        public async Task<IEnumerable<ProductOrder>> Handle(GetOrderInfoQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_gs.OrderInfo(request.Userid));
        }
    }
}
