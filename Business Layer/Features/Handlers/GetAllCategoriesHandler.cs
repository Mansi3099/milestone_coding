﻿using Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Features.Queries;
using Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Persistence;
using Mansi_Milestone_9Sept__2_55pm_.Entities;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Features.Handlers
{
    public class GetAllCategoriesHandler : IRequestHandler<GetAllCategoriesQuery, IEnumerable<Categories>>
    {
        private IGroceryServices _gs;

        public GetAllCategoriesHandler(IGroceryServices gs)
        {
            _gs = gs;
        }

        public async Task<IEnumerable<Categories>> Handle(GetAllCategoriesQuery request, CancellationToken cancellationToken)
        {
            return (IEnumerable<Categories>)await Task.FromResult(_gs.GetAllCategories());
        }
    }
}
