﻿using Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Features.Commands;
using Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Persistence;
using Mansi_Milestone_9Sept__2_55pm_.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Features.Handlers
{
    public class PlaceOrderCommandHandler : IRequestHandler<PlaceOrderCommand, ProductOrder>
    {
        private IGroceryServices _gs;

        public PlaceOrderCommandHandler(IGroceryServices gs)
        {
            _gs = gs;
        }

        public async Task<ProductOrder> Handle(PlaceOrderCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_gs.PlaceOrder(request.Userid, request.Productid));
        }
    }
}
