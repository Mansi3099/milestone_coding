﻿using Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Persistence;
using Mansi_Milestone_9Sept__2_55pm_.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
//This is Command Class
//It will Handle the Add operation

namespace Mansi_Milestone_9Sept__2_55pm_.Business_Layer.Features.Commands
{
    public class PlaceOrderCommand : IRequest<ProductOrder>
    {

        public int Userid { get; set; }

        public int Productid { get; set; }
    }
        
}
