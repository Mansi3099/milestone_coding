﻿using System;
using System.Collections.Generic;

namespace Mansi_Milestone_9Sept__2_55pm_.Entities
{
    public partial class ProductOrder
    {
        public int Orderid { get; set; }
        public int Productid { get; set; }
        public int Userid { get; set; }

        public virtual Product Product { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
