﻿using System;
using System.Collections.Generic;

namespace Mansi_Milestone_9Sept__2_55pm_.Entities
{
    public partial class MenuBar
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public bool? Openinnewwindow { get; set; }
    }
}
