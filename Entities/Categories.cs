﻿using System;
using System.Collections.Generic;

namespace Mansi_Milestone_9Sept__2_55pm_.Entities
{
    public partial class Categories
    {
        public Categories()
        {
            Product = new HashSet<Product>();
        }

        public int Catid { get; set; }
        public string Catname { get; set; }

        public virtual ICollection<Product> Product { get; set; }
    }
}
